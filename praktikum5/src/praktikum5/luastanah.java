package praktikum5;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class luastanah extends JFrame {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 30;
    private final JTextField text1;
    private final JTextField text2;
    private final JTextField text3;
    private final JLabel label1;
    private final JLabel label2;
    private final JLabel label3;
    private JButton button;

    public static void main(String[] args) {
        luastanah frame = new luastanah();
        frame.setVisible(true);

    }

    public luastanah() {
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(false);
        setLayout(null);
        setTitle("Luas Tanah");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        text1 = new JTextField();
        text1.setBounds(120, 5, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(text1);

        text2 = new JTextField();
        text2.setBounds(120, 50, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(text2);

        text3 = new JTextField();
        text3.setBounds(120, 80, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(text3);

        label1 = new JLabel("Panjang (m)");
        label1.setBounds(20, 5, 110, 30);
        contentPane.add(label1);

        label2 = new JLabel("Lebar(m)");
        label2.setBounds(20, 50, 110, 30);
        contentPane.add(label2);

        label3 = new JLabel("Luas(m2)");
        label3.setBounds(20, 80, 110, 30);
        contentPane.add(label3);

        button = new JButton("Hitung");
        button.setBounds(120, 125, 100, 30);
        contentPane.add(button);

        setDefaultCloseOperation(EXIT_ON_CLOSE);

        button.addActionListener(new ActionListener() {
            int bilan1, bilan2, hasill;

            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    bilan1 = Integer.parseInt(text1.getText());
                    bilan2 = Integer.parseInt(text2.getText());
                    hasill = bilan1 * bilan2;
                    text3.setText(Integer.toString(hasill));
                } catch (NumberFormatException ev) {
                    JOptionPane.showMessageDialog(null, "Maaf hanya Integer yang diperbolehkan!", "Error", JOptionPane.ERROR_MESSAGE);

                }
            }
        }
        );
        setVisible(true);
        setLocationRelativeTo(null);
    }
}
